import * as application from "@nativescript/core/application";
import * as purchase from "@proplugins/nativescript-purchase";

(global as any).purchaseInitPromise = purchase.init([
    "com.tangrainc.purchasesample.Product1",
    "com.tangrainc.purchasesample.Product2",
    "com.tangrainc.purchasesample.Sub1",
    "com.tangrainc.purchasesample.Sub2",
    "com.tangrainc.purcahsesample.Seasonal",
]);

application.run({ moduleName: "app-root" });
