import * as observable from "@nativescript/core/data/observable";
import * as pages from "@nativescript/core/ui/page";
import { ItemEventData } from "@nativescript/core/ui/list-view";

import * as purchase from "@proplugins/nativescript-purchase";
import { Transaction, TransactionState } from "@proplugins/nativescript-purchase/transaction";
import { Product } from "@proplugins/nativescript-purchase/product";

let viewModel: observable.Observable;

export function pageLoaded(args: observable.EventData) {
    const page = <pages.Page>args.object;

    viewModel = new observable.Observable();

    purchase.off(purchase.transactionUpdatedEvent);
    purchase.on(purchase.transactionUpdatedEvent, (transaction: Transaction) => {
        console.log(JSON.stringify(transaction));

        if (transaction.transactionState === TransactionState.Restored) {
            console.log(transaction.originalTransaction.transactionDate);
        }
        if (transaction.transactionState === TransactionState.Purchased
            && transaction.productIdentifier.indexOf(".product2") >= 0) {
            setTimeout(() => {
                purchase.consumePurchase(transaction.transactionReceipt)
                    .then(() => console.log("CONSUMED"))
                    .catch((e) => {
                        console.log(e);
                        if (e.toString().indexOf("again") >= 0) {
                            // Sometimes the API fails with `Server error, please try again`
                            return purchase.consumePurchase(transaction.transactionReceipt);
                        }
                    });
            });
        }
        if (transaction.transactionState === TransactionState.Restored
            && transaction.originalTransaction.productIdentifier.indexOf(".product2") >= 0) {
            purchase.consumePurchase(transaction.originalTransaction.transactionReceipt)
                .then(() => console.log("CONSUMED"))
                .catch(console.error);
        }
    });

    (global as any).purchaseInitPromise
        .then(() => purchase.getProducts())
        .then((res) => {
            viewModel.set("items", res);
        })
        .catch(console.error);

    page.bindingContext = viewModel;
}

export function onProductTap(data: ItemEventData) {
    let product = viewModel.get("items")[data.index] as Product;

    purchase.buyProduct(product, "tangra");
}

export function onRestoreTap() {
    purchase.restorePurchases().then(() => { console.log("Purchases restored!"); });
}