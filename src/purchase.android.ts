/*! *****************************************************************************
Copyright (c) 2019 Tangra Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
***************************************************************************** */

import * as application from "@nativescript/core/application";
import * as common from "./purchase-common";
import { Product } from "./product/product";
import { Transaction, TransactionState } from "./transaction/transaction";

export * from "./purchase-common";

let billingClient: com.android.billingclient.api.BillingClient;
let currentBuyPayload: string;
let currentBuyProductIdentifier: string;
let currentProductIdentifiers: Array<string>;

export function init(productIdentifiers: Array<string>): Promise<any> {
    return new Promise((resolve, reject) => {
        currentProductIdentifiers = productIdentifiers;

        ensureApplicationContext()
            .then(() => {
                billingClient = com.android.billingclient.api.BillingClient
                    .newBuilder(application.android.startActivity)
                    .enablePendingPurchases()
                    .setListener(new com.android.billingclient.api.PurchasesUpdatedListener({
                        onPurchasesUpdated(billingResult, purchases) {
                            if (billingResult.getResponseCode() === com.android.billingclient.api.BillingClient.BillingResponseCode.OK
                                && purchases) {
                                const purchasesSize = purchases.size();
                                for (let loop = 0; loop < purchasesSize; loop++) {
                                    const purchase = purchases.get(loop);
                                    acknowledgePurchase(purchase)
                                        .then(() => {
                                            const tran = new Transaction(purchase);
                                            // We need reset it, since otherwise we need to query the purchases in order to get the updated one from the store...
                                            tran.developerPayload = tran.developerPayload || currentBuyPayload;
                                            common._notify(common.transactionUpdatedEvent, tran);
                                        })
                                        .catch(console.error);
                                }
                            }
                            else if (billingResult.getResponseCode() === com.android.billingclient.api.BillingClient.BillingResponseCode.USER_CANCELED) {
                                common._notify(common.transactionUpdatedEvent, getFailedTransaction());
                            }
                            else {
                                console.error("Error when updating purchase", billingResult.getDebugMessage());
                            }
                        }
                    }))
                    .build();

                billingClient.startConnection(new com.android.billingclient.api.BillingClientStateListener({
                    onBillingSetupFinished(billingResult) {
                        if (billingResult.getResponseCode() !== com.android.billingclient.api.BillingClient.BillingResponseCode.OK) {
                            reject(new Error(`startConnection Error: ${billingResult.getDebugMessage()}`));
                        }
                        else {
                            resolve();
                        }
                    },
                    onBillingServiceDisconnected() {
                        // Try to restart the connection on the next request to
                        // Google Play by calling the startConnection() method.
                        // TODO: shoudl we do anything here???
                        console.error("Billing Service disconnected!");
                    }
                }));
            })
            .catch(reject);
    });
}

export function getProducts(): Promise<Array<Product>> {
    return new Promise<Array<Product>>((resolve, reject) => {
        Promise.all([
            getProductsByType(com.android.billingclient.api.BillingClient.SkuType.INAPP),
            getProductsByType(com.android.billingclient.api.BillingClient.SkuType.SUBS),
        ])
            .then((result: Array<java.util.List<com.android.billingclient.api.SkuDetails>>) => {
                const productArray: Array<Product> = [];
                for (let type = 0; type <= 1; type++) {
                    const resultSize = result[type].size();
                    for (let index = 0; index < resultSize; index++) {
                        const item = result[type].get(index);
                        productArray.push(new Product(item, (type === 0 ? "inapp" : "subs")));
                    }
                }

                resolve(productArray);
            })
            .catch(reject);
    });
}

export function buyProduct(product: Product, developerPayload?: string) {
    const tran = new Transaction(null);
    tran.transactionState = TransactionState.Purchasing;
    tran.productIdentifier = product.productIdentifier;
    tran.developerPayload = developerPayload;
    common._notify(common.transactionUpdatedEvent, tran);

    currentBuyProductIdentifier = product.productIdentifier;
    currentBuyPayload = developerPayload;

    const flowParams = com.android.billingclient.api.BillingFlowParams.newBuilder()
        .setSkuDetails(product.nativeValue)
        .build();
    billingClient.launchBillingFlow(application.android.foregroundActivity, flowParams);
}

export function consumePurchase(token: string): Promise<number> {
    return new Promise<number>((resolve, reject) => {
        const consumeParams = com.android.billingclient.api.ConsumeParams.newBuilder()
            .setPurchaseToken(token)
            .setDeveloperPayload(currentBuyPayload || "")
            .build();

        billingClient.consumeAsync(
            consumeParams,
            new com.android.billingclient.api.ConsumeResponseListener({
                onConsumeResponse(billingResult, purchaseToken) {
                    if (billingResult.getResponseCode() === com.android.billingclient.api.BillingClient.BillingResponseCode.OK) {
                        resolve();
                    }
                    else {
                        reject(new Error(billingResult.getDebugMessage()));
                    }
                }
            }),
        );
    });
}

export function restorePurchases(): Promise<void[]> {
    return Promise.all([
        restorePurchasesOfType(com.android.billingclient.api.BillingClient.SkuType.INAPP),
        restorePurchasesOfType(com.android.billingclient.api.BillingClient.SkuType.SUBS),
    ]);
}

export function canMakePayments(): boolean {
    return true;
}

function acknowledgePurchase(purchase: com.android.billingclient.api.Purchase) {
    if (purchase.isAcknowledged()) {
        return Promise.resolve();
    }

    return new Promise<void>((resolve, reject) => {
        const acknowledgePurchaseParams = com.android.billingclient.api.AcknowledgePurchaseParams.newBuilder()
            .setPurchaseToken(purchase.getPurchaseToken())
            .setDeveloperPayload(currentBuyPayload || "")
            .build();
        billingClient.acknowledgePurchase(
            acknowledgePurchaseParams,
            new com.android.billingclient.api.AcknowledgePurchaseResponseListener({
                onAcknowledgePurchaseResponse(billingResult) {
                    if (billingResult.getResponseCode() !== com.android.billingclient.api.BillingClient.BillingResponseCode.OK) {
                        console.error("Couldn't acknowledge purchase", billingResult.getDebugMessage());
                        console.warn("Retrying in 5 secs...");
                        setTimeout(() => { acknowledgePurchase(purchase).then(resolve, reject); }, 5000);
                    }
                    else {
                        resolve();
                    }
                }
            })
        );
    });
}

function getFailedTransaction() {
    const tran = new Transaction(null);
    tran.transactionState = TransactionState.Failed;
    tran.productIdentifier = currentBuyProductIdentifier;
    tran.developerPayload = currentBuyPayload;

    return tran;
}

function getProductsByType(productType: string): Promise<java.util.List<com.android.billingclient.api.SkuDetails>> {
    return new Promise<java.util.List<com.android.billingclient.api.SkuDetails>>((resolve, reject) => {
        const params = com.android.billingclient.api.SkuDetailsParams.newBuilder();
        const skuList = new java.util.ArrayList<string>();

        for (const item of currentProductIdentifiers) {
            skuList.add(item.toLowerCase()); // Android product IDs are all lower case
        }

        params.setSkusList(skuList).setType(productType);
        billingClient.querySkuDetailsAsync(
            params.build(),
            new com.android.billingclient.api.SkuDetailsResponseListener({
                onSkuDetailsResponse(billingResult, skuDetailsList) {
                    if (billingResult.getResponseCode() !== com.android.billingclient.api.BillingClient.BillingResponseCode.OK) {
                        reject(new Error(`querySkuDetailsAsync Error: ${billingResult.getDebugMessage()}`));
                    }
                    else {
                        if (skuDetailsList === null) {
                            resolve(null);
                            return;
                        }

                        resolve(skuDetailsList);
                    }
                }
            }),
        );
    });
}

function restorePurchasesOfType(type: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        const purchaseResponse = billingClient.queryPurchases(type);
        if (purchaseResponse.getResponseCode() !== com.android.billingclient.api.BillingClient.BillingResponseCode.OK) {
            reject(new Error(purchaseResponse.getBillingResult().getDebugMessage()));
            return;
        }

        const purchases = purchaseResponse.getPurchasesList();
        const purchasesSize = purchases.size();
        for (let loop = 0; loop < purchasesSize; loop++) {
            const tran = new Transaction(null);
            tran.originalTransaction = new Transaction(purchases.get(loop));
            tran.transactionState = TransactionState.Restored;

            common._notify(common.transactionUpdatedEvent, tran);
        }
    });
}

function ensureApplicationContext(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
        if (application.android && application.android.foregroundActivity) {
            resolve();
            return;
        }

        application.on(application.launchEvent, (args: application.LaunchEventData) => {
            resolve();
        });
    });
}