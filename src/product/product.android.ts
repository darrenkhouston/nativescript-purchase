/*! *****************************************************************************
Copyright (c) 2019 Tangra Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
***************************************************************************** */

import { ProductType } from ".";
import { ProductBase } from "./product-common";

export * from "./product-common";

export class Product extends ProductBase {
    constructor(nativeValue: com.android.billingclient.api.SkuDetails, type: ProductType) {
        super(nativeValue, type);
        
        this.productIdentifier = nativeValue.getSku();
        this.localizedDescription = nativeValue.getDescription();
        this.localizedTitle = nativeValue.getTitle();
        this.priceAmount = nativeValue.getPriceAmountMicros() / 1000000;
        this.priceFormatted = nativeValue.getPrice();
        this.priceCurrencyCode = nativeValue.getPriceCurrencyCode();
        if (nativeValue.getType() === com.android.billingclient.api.BillingClient.SkuType.SUBS) {
            this.subscriptionPeriod = nativeValue.getSubscriptionPeriod();
        }
    }
}