/*! *****************************************************************************
Copyright (c) 2019 Tangra Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
***************************************************************************** */

import { TransactionBase, TransactionState } from "./transaction-common";

export * from "./transaction-common";

export class Transaction extends TransactionBase {

    constructor(nativeValue: com.android.billingclient.api.Purchase) {
        super(nativeValue);
        
        if (nativeValue) {
            switch (nativeValue.getPurchaseState()) {
                case com.android.billingclient.api.Purchase.PurchaseState.PURCHASED:
                    this.transactionState = TransactionState.Purchased;
                    break;

                case com.android.billingclient.api.Purchase.PurchaseState.PENDING:
                    this.transactionState = TransactionState.Deferred;
                    break;

                default:
                    this.transactionState = TransactionState.Failed;
                    break;
            }

            this.productIdentifier = nativeValue.getSku();
            this.transactionReceipt = nativeValue.getPurchaseToken();
            this.dataSignature = nativeValue.getSignature();
            this.transactionIdentifier = nativeValue.getOrderId();
            this.developerPayload = nativeValue.getDeveloperPayload();

            const purchaseTime = nativeValue.getPurchaseTime();
            if (purchaseTime) {
                this.transactionDate = new Date(purchaseTime);
            }
        }
    }
} 